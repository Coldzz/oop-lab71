﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            // creating objects
            Human JustHuman = new Human("test", "test", "test");
            Graduant JustGraduant = new Graduant("Arsen", "Tsiurak", "04.05.2001", 7, 9, "School #1");
            Student JustStudent = new Student("Lupa", "Pupenko", "05.02.2000", "3", "Mechanik", "mechanical", "bursa 17");
            Teacher JustTeacher = new Teacher("Pupa", "Lupenko", "05.08.1900", "fizruk", "mechanical", "bursa 17");
            LibraryUser JustLibraryUser = new LibraryUser("Armen", "Bebeshun", "20.11.1998", 430, "30.06.2019", 34);
            
            //print all data
            Console.WriteLine("\tAll objects:");
            JustHuman.ShowInfo();
            JustGraduant.ShowInfo(); 
            JustStudent.ShowInfo();
            JustTeacher.ShowInfo();
            JustLibraryUser.ShowInfo();

            // default constructor
            Console.WriteLine("Default constructor");
            Teacher TestTeacher = new Teacher();
            TestTeacher.ShowInfo();

            // copy constructor
            Console.WriteLine("Copy constructor:");
            Teacher Test1Teacher = new Teacher(JustTeacher);
            Test1Teacher.ShowInfo();
            Console.ReadLine();
        }
    }
}
