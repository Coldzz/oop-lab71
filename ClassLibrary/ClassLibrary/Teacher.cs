﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Teacher : Human
    {
        public String Post { get; set; }
        public String Department { get; set; }
        public String WorkPlace { get; set; }

        public Teacher(String Name, String SurName, String BirthDate, String Post, String Department, String WorkPlace) : base(Name, SurName, BirthDate)
        {
            this.Post = Post;
            this.Department = Department;
            this.WorkPlace = WorkPlace;
        }

        public Teacher() : base()
        {
            this.Post = "Empty Post";
            this.Department = "Empty Department";
            this.WorkPlace = "Empty work Place";
        }

        public Teacher(Teacher fooTeacher) : base(fooTeacher)
        {
            this.Post = fooTeacher.Post;
            this.Department = fooTeacher.Department;
            this.WorkPlace = fooTeacher.WorkPlace;
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine("Post: {0}, Department: {1}, Workplace: {2}", Post, Department, WorkPlace);
        }


    }
}
