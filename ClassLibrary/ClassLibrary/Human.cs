﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Human
    {
        public String Name { get; set; }
        public String SurName { get; set; }
        public String BirthDate { get; set; }

        public Human()
        {
            this.Name = "Empty name";
            this.SurName = "Empty surname";
            this.BirthDate = "Empty birth date";
        }
        public Human(String Name, String SurName, String BirthDate)
        {
            this.Name = Name;
            this.SurName = SurName;
            this.BirthDate = BirthDate;
        }
        public Human(Human foohuman)
        {
            this.Name = foohuman.Name;
            this.SurName = foohuman.SurName;
            this.BirthDate = foohuman.BirthDate;
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine("Name: {0}, Surname: {1}, Birthday: {2};", Name, SurName, BirthDate);
        }
    }
}
