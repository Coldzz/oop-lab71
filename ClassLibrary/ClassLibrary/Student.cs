﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Student : Human
    {
        public String Course { get; set; }
        public String Group { get; set; }
        public String Faculty { get; set; }
        public String InstitutionName { get; set; }


        public Student(String Name, String SurName, String BirthDate, String Course, String Group, String Faculty, String InstitutionName) : base(Name, SurName, BirthDate)
        {
            this.Course = Course;
            this.Group = Group;
            this.Faculty = Faculty;
            this.InstitutionName = InstitutionName;
        }

        public Student() : base()
        {
            this.Course = "Empty course";
            this.Group = "Empty group";
            this.Faculty = "Empty faculty";
            this.InstitutionName = "Empty institution name";
        }

        public Student(Student fooStudent) : base(fooStudent)
        {
            this.Course = fooStudent.Course;
            this.Group = fooStudent.Group;
            this.Faculty = fooStudent.Faculty;
            this.InstitutionName = fooStudent.InstitutionName;
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine("Course: {0}, Group: {1}, Faculty: {2}, Institute: {3}", Course, Group, Faculty, InstitutionName);
        }


    }
}
