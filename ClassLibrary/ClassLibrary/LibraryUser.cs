﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{

    public class LibraryUser : Human
    {
        public int ReaderID { get; set; }
        public String EndReadersIdDate { get; set; }
        public int MonthlyPay { get; set; }

        public LibraryUser(String Name, String SurName, String BirthDate, int ReaderID, String EndReaderid, int MounthlyPat) : base(Name, SurName, BirthDate)
        {
            this.ReaderID = ReaderID;
            this.EndReadersIdDate= EndReaderid;
            this.MonthlyPay = MonthlyPay;
        }

        public LibraryUser() : base()
        {
            this.ReaderID = 0;
            this.EndReadersIdDate = "Empty readers id data";
            this.MonthlyPay = 0;
        }

        public LibraryUser(LibraryUser fooLibra) : base(fooLibra)
        {
            this.ReaderID = fooLibra.ReaderID;
            this.EndReadersIdDate = fooLibra.EndReadersIdDate;
            this.MonthlyPay = fooLibra.MonthlyPay;            
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine("Reader`s id: {0}, End reader id cart date: {1}, Mounthly payy {2}" , ReaderID, EndReadersIdDate, MonthlyPay);
        }


    }
}
