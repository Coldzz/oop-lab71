﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Graduant : Human 
    {
        public int ZnoResult { get; set; }
        public int SchoolResult { get; set; }
        public String SchoolName { get; set; }


        public Graduant(String Name, String SurName, String BirthDate, int ZnoResult, int SchoolResult, String SchoolName) : base(Name, SurName, BirthDate)
        {
            this.ZnoResult = ZnoResult;
            this.SchoolResult = SchoolResult;
            this.SchoolName = SchoolName;
        }

        public Graduant() : base()
        {
            this.ZnoResult = 0;
            this.SchoolResult = 0;
            this.SchoolName = "";
        }

        public Graduant(Graduant fooGraduant) : base(fooGraduant)
        {
            this.ZnoResult = fooGraduant.ZnoResult;
            this.SchoolResult = fooGraduant.SchoolResult;
            this.SchoolName = fooGraduant.SchoolName;
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine("ZNO result: {0}, Study result: {1}, School name: {2}", ZnoResult, SchoolResult, SchoolName);
        }


    }
}
